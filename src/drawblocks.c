#include "drawblocks.h"
#include <time.h>
#include <stdio.h>

void initBlocks(TT_Block *block, TT_Texture blockTex[], int colorType, SDL_Rect *walls, int wallNum, int x, int y){
    SDL_memset(block, 0, sizeof (TT_Block));
    SDL_Log("TT_Block size=%d\n", sizeof (TT_Block));
    block->x = x;
    block->y = y;
    for(int i = 0; i < TT_BLOCK_COLOR_NUM; i++){
        block->tex[i] = blockTex[i];
    }
    block->colorType = colorType;
    block->nextColorType = colorType;
    block->type = random(TT_BLOCK_TYPE_NUM);
    block->nextType = random(TT_BLOCK_TYPE_NUM);
#if FOR_DOUDOU_PLAY == 1
    block->type = 3;
    block->nextType = 3;
#endif
    block->bShown = false;
    block->bAlive = true;
    block->bStartGame = false;
    block->walls = walls;
    block->wallNum = wallNum;
    calBlocksNum(block);
    calBlockborderOffsetXY(block);
}

void reInitBlocks(TT_Block *block, int x, int y){
    block->x = x;
    block->y = y;
    block->type = block->nextType;
    block->colorType = block->nextColorType;
    block->nextType = random(TT_BLOCK_TYPE_NUM);
#if FOR_DOUDOU_PLAY == 1
    block->type = 3;
    block->nextType = 3;
#endif
    block->nextColorType = random(TT_BLOCK_COLOR_NUM) + 1;
    block->curTransState = 0;
    block->bShown = true;
    block->bAlive = true;
    calBlocksNum(block);
    calBlockborderOffsetXY(block);
}

void reStartGame(TT_Block *block){
    if(block->bStartGame == true){
        return;
    }
    SDL_memset(TT_RemainBlocks, 0, sizeof (TT_RemainBlocks) * sizeof (int));
    SDL_memset(TT_WallsBuff, 0, sizeof (SDL_Rect));
    reInitBlocks(block, 200, 100);
    block->remainBlockHeight = 0;
    block->remainBlockWidth = 0;
    block->wallNum = 0;
    block->collisionWall = 0;
    block->score = 0;
    block->bStartGame = true;
    block->bShown = true;
    block->bAlive = true;
    playSound(block->startSound);
}

void startGame(TT_Block *block){
    if(block->bStartGame == false){
        playSound(block->startSound);
    }
    block->bStartGame = true;
    block->bShown = true;
}

int random(int m){
    srand((unsigned)time(NULL));
    return rand() % m;
}

void copyDeadBlocksToRemainBlocks(TT_Block *block){
    if(block->bAlive){
        return;
    }
    int x = block->x;
    int y = block->y;
    for(int i = 0; i < TT_BLOCK_NUM_AREA_HEIGHT; i++){
        x = block->x;
        for(int j = 0; j < TT_BLOCK_NUM_AREA_WIDTH; j++){
            if(TT_Blocks[block->type][block->curTransState][i][j] >= 1){
                TT_RemainBlocks[(y - TT_BLOCK_MOVE_AREA_START_Y) / TT_BLOCK_HEIGHT][(x - TT_BLOCK_MOVE_AREA_START_X) / TT_BLOCK_WIDTH] = block->colorType;
            }
            x += TT_BLOCK_WIDTH;
        }
        y += TT_BLOCK_HEIGHT;
    }
    calRemainBlocksSize(block);
}

void calRemainBlocksSize(TT_Block *block){
    if(block->remainBlockHeight >= TT_BACKGROUND_HEIGHT){
        return;
    }
    int minX = TT_BLOCK_NUM_BACKGROUND_WIDTH;
    int minY = TT_BLOCK_NUM_BACKGROUND_HEIGHT;
    int maxX = 0;
    int maxY = 0;
    for(int i = 0; i < TT_BLOCK_NUM_BACKGROUND_HEIGHT; i++){
        for(int j = 0; j < TT_BLOCK_NUM_BACKGROUND_WIDTH; j++){
            if(TT_RemainBlocks[i][j] >= 1){
                if(j > maxX){
                    maxX = j;
                }
                if(i > maxY){
                    maxY = i;
                }
                if(j < minX){
                    minX = j;
                }
                if(i < minY){
                    minY = i;
                }
            }
        }
    }
    block->remainBlockWidth = (maxX + 1 - minX) * TT_BLOCK_WIDTH;
    block->remainBlockHeight = (maxY + 1 - minY) * TT_BLOCK_WIDTH;

    SDL_Log("r.w=%d, r.h=%d\n", block->remainBlockWidth, block->remainBlockHeight);
}

void deleteCompletedBlocks(TT_Block *block){
    if(block->remainBlockWidth < TT_BACKGROUND_WIDTH){
        return;
    }
    int width = 0;
    int height = 0;
    int rowFlag = 0;
    int tmp[TT_BLOCK_NUM_BACKGROUND_HEIGHT][TT_BLOCK_NUM_BACKGROUND_WIDTH];
    SDL_memset(tmp, 0, sizeof (tmp));
    for(int i = 0; i < TT_BLOCK_NUM_BACKGROUND_HEIGHT; i++){
        width = 0;
        for(int j = 0; j < TT_BLOCK_NUM_BACKGROUND_WIDTH; j++){
            if(TT_RemainBlocks[i][j] >= 1){
                width += 1;
                if(width == TT_BLOCK_NUM_BACKGROUND_WIDTH){
                    for(int k = 0; k < TT_BLOCK_NUM_BACKGROUND_WIDTH; k++){
                        TT_RemainBlocks[i][k] = 0;
                    }
                    block->score++;
                    if(block->score > block->maxScore){
                        block->maxScore = block->score;
                    }
                    playSound(block->completeSound);
                }
            }
        }
    }
    height = TT_BLOCK_NUM_BACKGROUND_HEIGHT - 1;
    for(int i = TT_BLOCK_NUM_BACKGROUND_HEIGHT - 1; i > 0; i--){
        for(int j = 0; j < TT_BLOCK_NUM_BACKGROUND_WIDTH; j++){
            if(TT_RemainBlocks[i][j] >= 1){
                tmp[height][j] = TT_RemainBlocks[i][j];
                rowFlag = 1;
            }
        }
        if(rowFlag == 1){
            --height;
            rowFlag = 0;
        }
    }
    SDL_memcpy(TT_RemainBlocks, tmp, sizeof (TT_RemainBlocks));

    calRemainBlocksSize(block);
}

TT_Bool isGameOver(TT_Block *block){
    if(block->remainBlockHeight >= TT_BACKGROUND_HEIGHT){
        block->bStartGame = false;
        block->bAlive = false;
        return true;
    }
    return false;
}

TT_Bool isBlockDead(TT_Block *block){
    return !block->bAlive;
}

void updateWalls(TT_Block *block){
    int i, j, k = 0;
    int x = TT_BLOCK_MOVE_AREA_START_X;
    int y = TT_BLOCK_MOVE_AREA_START_Y;
    SDL_Rect wall;
    for(i = 0; i < TT_BLOCK_NUM_BACKGROUND_HEIGHT; i++){
        x = TT_BLOCK_MOVE_AREA_START_X;
        for(j = 0; j < TT_BLOCK_NUM_BACKGROUND_WIDTH; j++){
            if(TT_RemainBlocks[i][j] >= 1){
                wall.x = x;
                wall.y = y;
                wall.w = TT_BLOCK_WIDTH;
                wall.h = TT_BLOCK_HEIGHT;
                TT_WallsBuff[k++] = wall;
            }
            x += TT_BLOCK_WIDTH;
        }
        y += TT_BLOCK_HEIGHT;
    }
    block->wallNum = k;
}

void calBlockborderOffsetXY(TT_Block *block){
    int minX = TT_BLOCK_NUM_AREA_WIDTH;
    int minY = TT_BLOCK_NUM_AREA_HEIGHT;
    int maxX = 0;
    int maxY = 0;
    for(int i = 0; i < TT_BLOCK_NUM_AREA_HEIGHT; i++){
        for(int j = 0; j < TT_BLOCK_NUM_AREA_WIDTH; j++){
            if(TT_Blocks[block->type][block->curTransState][i][j] >= 1){
                if(j > maxX){
                    maxX = j;
                }
                if(i > maxY){
                    maxY = i;
                }
                if(j < minX){
                    minX = j;
                }
                if(i < minY){
                    minY = i;
                }
            }
        }
    }
    block->borderOffsetX1 = minX * TT_BLOCK_WIDTH;
    block->borderOffsetY1 = minY * TT_BLOCK_HEIGHT;
    block->borderOffsetX2 = (maxX + 1) * TT_BLOCK_WIDTH;
    block->borderOffsetY2 = (maxY + 1) * TT_BLOCK_HEIGHT;

    block->curWidth = block->borderOffsetX2 - block->borderOffsetX1;
    block->curHeight = block->borderOffsetY2 - block->borderOffsetY1;
}

int calBlocksNum(TT_Block *block){
    int blocksNum = 0;
    for(int i = 0; i < TT_BLOCK_NUM_AREA_HEIGHT; i++){
        for(int j = 0; j < TT_BLOCK_NUM_AREA_WIDTH; j++){
            if(TT_Blocks[block->type][block->curTransState][i][j] >= 1){
                blocksNum++;
            }
        }
    }
    block->blocksNum = blocksNum;
    return blocksNum;
}

void drawBlocks(TT_Window *win, TT_Block *block){
    if(block->bShown == false){
        return;
    }
    int i, j;
    int x = block->x;
    int y = block->y;
    for(i = 0; i < TT_BLOCK_NUM_AREA_HEIGHT; i++){
        x = block->x;
        for(j = 0; j < TT_BLOCK_NUM_AREA_WIDTH; j++){
            if(TT_Blocks[block->type][block->curTransState][i][j] >= 1){
                renderImageFromTexture(win, &block->tex[block->colorType - 1], NULL, x, y);
            }
            x += TT_BLOCK_WIDTH;
        }
        y += TT_BLOCK_HEIGHT;
    }
}

void drawNextBlocks(TT_Window *win, TT_Block *block, int px, int py){
    int i, j;
    int x = px;
    int y = py;
    for(i = 0; i < TT_BLOCK_NUM_AREA_HEIGHT; i++){
        x = px;
        for(j = 0; j < TT_BLOCK_NUM_AREA_WIDTH; j++){
            if(TT_Blocks[block->nextType][0][i][j] >= 1){
                renderImageFromTexture(win, &block->tex[block->nextColorType - 1], NULL, x, y);
            }
            x += TT_BLOCK_WIDTH;
        }
        y += TT_BLOCK_HEIGHT;
    }
}

void transBlocks(TT_Block *block){
    if(block->bAlive == false){
        return;
    }
    int lastTransState = block->curTransState;
    if(block->curTransState < TT_BLOCK_TRANS_NUM - 1){
        block->curTransState++;
    }else{
        block->curTransState = 0;
    }
    calBlockborderOffsetXY(block);
    if(checkBlockCollision(block)){
       block->curTransState = lastTransState;
       calBlockborderOffsetXY(block);
    }
    playSound(block->transSound);
}

void moveBlocks(TT_Block *block){
    if(block->bAlive == false || block->bStartGame == false){
        return;
    }
    block->x += block->speedX;
    if(block->walls == NULL){
        if(block->x + block->borderOffsetX1 < TT_BLOCK_MOVE_AREA_START_X || block->x + block->borderOffsetX1 > (TT_BLOCK_MOVE_AREA_START_X + TT_BLOCK_MOVE_AREA_WIDTH - block->curWidth)){
            block->x -= block->speedX;
        }
        block->y += block->speedY;
        if(block->y + block->borderOffsetY1 < TT_BLOCK_MOVE_AREA_START_Y || block->y + block->borderOffsetY1 > (TT_BLOCK_MOVE_AREA_START_Y + TT_BLOCK_MOVE_AREA_HEIGHT - block->curHeight)){
            block->y -= block->speedY;            
        }
    }else{
        if(checkBlockCollision(block)){
            block->x -= block->speedX;
        }
        block->y += block->speedY;
        if(checkBlockCollision(block)){
            checkBlockAlive(block);
            block->y -= block->speedY;
        }
    }

#if FOR_DOUDOU_PLAY == 1
    block->speedX = 0;
#endif

}

void moveDownBlocks(TT_Block *block){
    if(block->bAlive == false || block->bStartGame == false){
        return;
    }
    do{
        block->y += TT_BLOCK_HEIGHT;
    } while (!checkBlockCollision(block));

    block->bAlive = false;
    block->y -= TT_BLOCK_HEIGHT;
}

void autoMoveDownBlocks(TT_Block *block){
    if(block->bAlive == false || block->bStartGame == false){
        return;
    }
    if(block->walls == NULL){
        block->y += TT_BLOCK_HEIGHT;
        if(block->y + block->borderOffsetY1 < TT_BLOCK_MOVE_AREA_START_Y || block->y + block->borderOffsetY1 > (TT_BLOCK_MOVE_AREA_START_Y + TT_BLOCK_MOVE_AREA_HEIGHT - block->curHeight)){
            block->y -= TT_BLOCK_HEIGHT;
        }
    }else{
        block->y += TT_BLOCK_HEIGHT;
        if(checkBlockCollision(block)){
            block->bAlive = false;
            block->y -= TT_BLOCK_HEIGHT;
        }
    }
}

TT_Bool checkBlockAlive(TT_Block *block){
    if(block->speedY > 0){
        //SDL_Log("dead at here!\n");
        block->bAlive = false;
    }
    return block->bAlive;
}

TT_Bool checkBlockCollision(TT_Block *block){
    int wallX1, wallY1, wallX2, wallY2;
    int blockX1, blockY1, blockX2, blockY2;

    if((block->x + block->borderOffsetX1) < TT_BLOCK_MOVE_AREA_START_X || (block->x + block->borderOffsetX1) > (TT_BLOCK_MOVE_AREA_START_X + TT_BLOCK_MOVE_AREA_WIDTH - block->curWidth)
       || (block->y + block->borderOffsetY1) < TT_BLOCK_MOVE_AREA_START_Y || (block->y + block->borderOffsetY1) > (TT_BLOCK_MOVE_AREA_START_Y + TT_BLOCK_MOVE_AREA_HEIGHT - block->curHeight)){
        SDL_Log("collison1!\n");        
        return true;
    }

    for(int k = 0; k < block->wallNum; k++){
        wallX1 = block->walls[k].x;
        wallY1 = block->walls[k].y;
        wallX2 = wallX1 + block->walls[k].w;
        wallY2 = wallY1 + block->walls[k].h;

        int x = block->x;
        int y = block->y;
        for(int i = 0; i < TT_BLOCK_NUM_AREA_HEIGHT; i++){
            x = block->x;
            for(int j = 0; j < TT_BLOCK_NUM_AREA_WIDTH; j++){
                if(TT_Blocks[block->type][block->curTransState][i][j] >= 1){
                    blockX1 = x;
                    blockY1 = y;
                    blockX2 = x + TT_BLOCK_WIDTH;
                    blockY2 = y + TT_BLOCK_HEIGHT;
                    /*矩形的四个点中任意一个落入另外一个矩形之中，视为碰撞*/
                    if((blockX1 > wallX1 && blockX1 < wallX2 && blockY1 > wallY1 && blockY1 < wallY2) ||
                       (blockX2 > wallX1 && blockX2 < wallX2 && blockY1 > wallY1 && blockY1 < wallY2) ||
                       (blockX1 > wallX1 && blockX1 < wallX2 && blockY2 > wallY1 && blockY2 < wallY2) ||
                       (blockX2 > wallX1 && blockX2 < wallX2 && blockY2 > wallY1 && blockY2 < wallY2)){
                        block->collisionWall = k;
                        SDL_Log("collison2!\n");
                        return true;
                    }
                    /*左右齐平和上下齐平和重合的情况*/
                    if((blockX1 == wallX1 && blockY1 > wallY1 && blockY1 < wallY2) ||
                       (blockX1 == wallX1 && blockY2 > wallY1 && blockY2 < wallY2) ||
                       (blockY1 == wallY1 && blockX1 > wallX1 && blockX1 < wallX2) ||
                       (blockY1 == wallY1 && blockX2 < wallX2 && blockX2 > wallX1) ||
                       (blockX1 == wallX1 && blockY1 == wallY1)){
                        SDL_Log("collison3!\n");
                        SDL_Log("block.x=%d, block.y=%d, block.curHeight=%d, wall.y=%d\n", block->x, block->y, block->curHeight, block->walls[block->collisionWall].y);
                        block->collisionWall = k;
                        return true;
                    }
                }
                x += TT_BLOCK_WIDTH;
            }
            y += TT_BLOCK_HEIGHT;
        }
    }

    return false;
}

void drawRemainBlocks(TT_Window *win, TT_Block *block){
    int i, j;
    int x = TT_BLOCK_MOVE_AREA_START_X;
    int y = TT_BLOCK_MOVE_AREA_START_Y;
    for(i = 0; i < TT_BLOCK_NUM_BACKGROUND_HEIGHT; i++){
        x = TT_BLOCK_MOVE_AREA_START_X;
        for(j = 0; j < TT_BLOCK_NUM_BACKGROUND_WIDTH; j++){
            if(TT_RemainBlocks[i][j] >= 1){
                renderImageFromTexture(win, &block->tex[TT_RemainBlocks[i][j] - 1], NULL, x, y);
            }
            x += TT_BLOCK_WIDTH;
        }
        y += TT_BLOCK_HEIGHT;
    }
}

void playSound(Mix_Chunk *sound){
    if(sound){
        Mix_PlayChannel(-1, sound, 0);
    }
}

void blocksHandle(TT_Block *block, SDL_Event *e){
    if(e->type == SDL_KEYDOWN && e->key.repeat == 0){
        switch (e->key.keysym.sym) {
        case SDLK_UP:
            transBlocks(block);
            break;
        case SDLK_DOWN:
            block->speedY += TT_BLOCK_SPEEDRATE_Y;
            playSound(block->moveSound);
            break;
        case SDLK_LEFT:
            if(block->speedX == 0) block->speedX -= TT_BLOCK_SPEEDRATE_X;
            playSound(block->moveSound);
            break;
        case SDLK_RIGHT:
            if(block->speedX == 0) block->speedX += TT_BLOCK_SPEEDRATE_X;
            playSound(block->moveSound);
            break;
        case SDLK_SPACE:
            moveDownBlocks(block);
            break;
        case SDLK_s:
            startGame(block);
            break;
        case SDLK_r:            
            reStartGame(block);
            break;
        }
    }
    if(e->type == SDL_KEYUP && e->key.repeat == 0){
        switch (e->key.keysym.sym) {
        case SDLK_DOWN:
            block->speedY -= TT_BLOCK_SPEEDRATE_Y;
            break;
        case SDLK_LEFT:
            if(block->speedX != 0) block->speedX += TT_BLOCK_SPEEDRATE_X;
            break;
        case SDLK_RIGHT:
            if(block->speedX != 0) block->speedX -= TT_BLOCK_SPEEDRATE_X;
            break;
        }
    }
}
