#include "render.h"

TT_Bool renderImageFromTexture(TT_Window *win, TT_Texture *tex, SDL_Rect *clip, int x, int y){
    TT_Bool success = true;
    SDL_Rect dstRect = {x, y, tex->width, tex->height};
    if(clip){
        dstRect.w = clip->w;
        dstRect.h = clip->h;
    }
    if(SDL_RenderCopy(win->render, tex->texture, clip, &dstRect) != 0){
        SDL_Log("SDL_RenderCopy failed: %s\n", SDL_GetError());
        success =  false;
    }

    return success;
}

TT_Bool renderText(TT_Window *win, TTF_Font *font, SDL_Color *color, char *text, int x, int y){
    SDL_Surface *surface = TTF_RenderText_Solid(font, text, *color);
    if(!surface){
        SDL_Log("TTF render text solid failed: %s\n", TTF_GetError());
        return false;
    }
    win->texture = SDL_CreateTextureFromSurface(win->render, surface);
    if(!(win->texture)){
        SDL_Log("SDL create texture from surface failed: %s\n", SDL_GetError());
        return false;
    }
    SDL_Rect dstRect = {x, y, surface->w, surface->h};
    SDL_FreeSurface(surface);
    SDL_RenderCopy(win->render, win->texture, NULL, &dstRect);
    SDL_DestroyTexture(win->texture);
    win->texture = NULL;
    return true;
}
