#include "texture.h"
#include "SDL_image.h"

TT_Texture loadImageTexture(TT_Window *win, const char *imgPath){
    TT_Texture texture = {
        .texture = NULL,
        .width = 0,
        .height = 0
    };
    SDL_Surface *surface = IMG_Load(imgPath);
    if(!surface){
        SDL_Log("Failed to load image %s: %s\n", imgPath, IMG_GetError());
    }else{
        SDL_SetColorKey(surface, SDL_TRUE, SDL_MapRGB(surface->format, 0xff, 0xff, 0xff));
        texture.texture = SDL_CreateTextureFromSurface(win->render, surface);
        if(!texture.texture){
            SDL_Log("Failed to load image %s: %s\n", imgPath, SDL_GetError());
        }
        texture.width = surface->w;
        texture.height = surface->h;
        SDL_FreeSurface(surface);
    }
    return texture;
}

void freeTexture(TT_Texture *tex){
    SDL_DestroyTexture(tex->texture);
    tex->texture = NULL;
    tex->width = 0;
    tex->height = 0;
}
