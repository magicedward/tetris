#ifndef WININIT_H
#define WININIT_H

#include "SDL.h"
#include "tetris.h"

TT_Bool initWindow(TT_Window *win);

#endif //WININIT_H
