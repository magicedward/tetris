#ifndef TEXTURE_H
#define TEXTURE_H

#include "SDL.h"
#include "tetris.h"

TT_Texture loadImageTexture(TT_Window *win, const char *imgPath);
void freeTexture(TT_Texture *tex);

#endif // TEXTURE_H
