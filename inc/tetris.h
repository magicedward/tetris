#ifndef TETRIS_H
#define TETRIS_H

#include "SDL.h"
#include "SDL_mixer.h"

#define FOR_DOUDOU_PLAY  (0)

#define TT_UNUSED(name) (void)(name)

#define TT_WINDOW_WIDTH 400
#define TT_WINDOW_HEIGHT 640

#define TT_IMG_PATH "../img/"
#define TT_FONT_PATH "../font/"
#define TT_SOUND_PATH "../sound/"

#define TT_BLOCK_WIDTH 20
#define TT_BLOCK_HEIGHT 20

#define TT_BLOCK_TRANS_NUM 4
#define TT_BLOCK_TYPE_NUM 6
#define TT_BLOCK_NUM_AREA_WIDTH 4
#define TT_BLOCK_NUM_AREA_HEIGHT 4

#define TT_BLOCK_NUM_BACKGROUND_WIDTH (10)
#define TT_BLOCK_NUM_BACKGROUND_HEIGHT (20)

#define TT_BACKGROUND_WIDTH (TT_BLOCK_NUM_BACKGROUND_WIDTH*TT_BLOCK_WIDTH)
#define TT_BACKGROUND_HEIGHT (TT_BLOCK_NUM_BACKGROUND_HEIGHT*TT_BLOCK_HEIGHT)

#define TT_BLOCK_TYPE_AREA_WIDTH (TT_BLOCK_WIDTH*TT_BLOCK_NUM_AREA_WIDTH)
#define TT_BLOCK_TYPE_AREA_HEIGHT (TT_BLOCK_HEIGHT*TT_BLOCK_NUM_AREA_HEIGHT)

#define TT_BLOCK_MOVE_AREA_START_X 100
#define TT_BLOCK_MOVE_AREA_START_Y 100

#define TT_BLOCK_MOVE_AREA_WIDTH TT_BACKGROUND_WIDTH
#define TT_BLOCK_MOVE_AREA_HEIGHT TT_BACKGROUND_HEIGHT

#define TT_BLOCK_TYPE_I 0
#define TT_BLOCK_TYPE_L 1
#define TT_BLOCK_TYPE_T 2

#define TT_BLOCK_SPEEDRATE_X TT_BLOCK_WIDTH
#define TT_BLOCK_SPEEDRATE_Y TT_BLOCK_HEIGHT

#define TT_MAX_WALLS (TT_BLOCK_NUM_BACKGROUND_WIDTH*TT_BLOCK_NUM_BACKGROUND_HEIGHT)
#define TT_MAX_BLOCKS (TT_BLOCK_NUM_BACKGROUND_WIDTH*TT_BLOCK_NUM_BACKGROUND_HEIGHT)

#define TT_NEXT_BLOCKS_X 170
#define TT_NEXT_BLOCKS_Y 34

#define TT_BLOCK_COLOR_NUM 4
#define TT_BLOCK_GREEN  1
#define TT_BLOCK_RED    2
#define TT_BLOCK_BLUE   3
#define TT_BLOCK_YELLOW 4

typedef enum {
    false = 0,
    true = 1,
} TT_Bool;

typedef struct {
    SDL_Window *win;
    SDL_Renderer *render;
    SDL_Texture *texture;
    int width;
    int height;
} TT_Window;

typedef struct {
    SDL_Texture *texture;
    int width;
    int height;
} TT_Texture;

typedef struct {
    int type;
    int nextType;
    int curTransState;
    int blocksNum;
    int x;
    int y;
    int borderOffsetX1;
    int borderOffsetY1;
    int borderOffsetX2;
    int borderOffsetY2;
    int curHeight;
    int curWidth;
    int speedX;
    int speedY;
    int remainBlockWidth;
    int remainBlockHeight;
    SDL_Rect *walls;
    int wallNum;
    int collisionWall;
    int score;
    int maxScore;
    int colorType;
    int nextColorType;
    TT_Bool bShown;
    TT_Bool bAlive;
    TT_Bool bStartGame;
    TT_Texture tex[TT_BLOCK_COLOR_NUM];
    Mix_Chunk *putSound;
    Mix_Chunk *moveSound;
    Mix_Chunk *transSound;
    Mix_Chunk *startSound;
    Mix_Chunk *completeSound;
} TT_Block;

typedef enum {
    left,
    right,
    down,
    up,
} TT_Direction;







#endif //TETRIS_H
