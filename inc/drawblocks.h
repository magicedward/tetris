#ifndef DRAWBLOCKS_H
#define DRAWBLOCKS_H

#include "tetris.h"
#include "blocks.h"
#include "render.h"
#include "texture.h"

void drawBlocks(TT_Window *win, TT_Block *block);
void transBlocks(TT_Block *block);
void initBlocks(TT_Block *block, TT_Texture blockTex[], int colorType, SDL_Rect *walls, int wallNum, int x, int y);
void moveBlocks(TT_Block *block);
void drawRemainBlocks(TT_Window *win, TT_Block *block);
TT_Bool checkBlockCollision(TT_Block *block);
void blocksHandle(TT_Block *block, SDL_Event *e);
int calBlocksNum(TT_Block *block);
void updateWalls(TT_Block *block);
int random(int m);
void copyDeadBlocksToRemainBlocks(TT_Block *block);
TT_Bool isBlockDead(TT_Block *block);
void reInitBlocks(TT_Block *block, int x, int y);
TT_Bool checkBlockAlive(TT_Block *block);
void calRemainBlocksSize(TT_Block *block);
void deleteCompletedBlocks(TT_Block *block);
TT_Bool isGameOver(TT_Block *block);
void calBlockborderOffsetXY(TT_Block *block);
void autoMoveDownBlocks(TT_Block *block);
void moveDownBlocks(TT_Block *block);
void drawNextBlocks(TT_Window *win, TT_Block *block, int px, int py);
void reStartGame(TT_Block *block);
void startGame(TT_Block *block);
void playSound(Mix_Chunk *sound);

#endif // DRAWBLOCKS_H
