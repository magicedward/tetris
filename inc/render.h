#ifndef RENDER_H
#define RENDER_H

#include "SDL.h"
#include "tetris.h"
#include "winInit.h"
#include "SDL_ttf.h"

TT_Bool renderImageFromTexture(TT_Window *win, TT_Texture *tex, SDL_Rect *clip, int x, int y);
TT_Bool renderText(TT_Window *win, TTF_Font *font, SDL_Color *color, char *text, int x, int y);

#endif // RENDER_H
