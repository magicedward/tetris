TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

INCLUDEPATH += $$PWD/inc

SOURCES += \
        main.c \
        src/blocks.c \
        src/drawblocks.c \
        src/render.c \
        src/texture.c \
        src/winInit.c

win32: LIBS += -L$$PWD/lib/SDL2-2.0.12/i686-w64-mingw32/lib/ -lmingw32 -lSDL2main -lSDL2

INCLUDEPATH += $$PWD/lib/SDL2-2.0.12/i686-w64-mingw32/include/SDL2
DEPENDPATH += $$PWD/lib/SDL2-2.0.12/i686-w64-mingw32/include/SDL2

HEADERS += \
    inc/blocks.h \
    inc/drawblocks.h \
    inc/render.h \
    inc/tetris.h \
    inc/texture.h \
    inc/winInit.h

win32: LIBS += -L$$PWD/lib/SDL2_image-2.0.5/i686-w64-mingw32/lib/ -lSDL2_image

INCLUDEPATH += $$PWD/lib/SDL2_image-2.0.5/i686-w64-mingw32/include/SDL2
DEPENDPATH += $$PWD/lib/SDL2_image-2.0.5/i686-w64-mingw32/include/SDL2

win32: LIBS += -L$$PWD/lib/SDL2_ttf-2.0.15/i686-w64-mingw32/lib/ -lSDL2_ttf

INCLUDEPATH += $$PWD/lib/SDL2_ttf-2.0.15/i686-w64-mingw32/include/SDL2
DEPENDPATH += $$PWD/lib/SDL2_ttf-2.0.15/i686-w64-mingw32/include/SDL2

win32: LIBS += -L$$PWD/lib/SDL2_mixer-2.0.4/i686-w64-mingw32/lib/ -lSDL2_mixer

INCLUDEPATH += $$PWD/lib/SDL2_mixer-2.0.4/i686-w64-mingw32/include/SDL2
DEPENDPATH += $$PWD/lib/SDL2_mixer-2.0.4/i686-w64-mingw32/include/SDL2
