# tetris

#### 介绍

基于SDL2的俄罗斯方块，开发环境是VScode

#### 依赖的库

1.  SDL2-2.0.12
2.  SDL2_image-2.0.5
3.  SDL2_ttf-2.0.15
4.  SDL2_mixer-2.0.4

#### 使用说明

1.  下载VScode + cmake + mingw64
2.  克隆本仓库：`git clone https://gitee.com:magicedward/tetris.git`
3.  分别复制依赖库的dll文件到build相关文件夹下面（如：复制tetris\lib\SDL2-2.0.12\i686-w64-mingw32\bin下面的SDL2.dll到QtCreator所配置的build文件夹下面的debug目录下）
4.  修改资源路径：
```
#define TT_IMG_PATH "yourPath/tetris/img/"
#define TT_FONT_PATH "yourPath/tetris/font/"
#define TT_SOUND_PATH "yourPath/tetris/sound/"
```

5. camke 编译运行 或f5debug 


